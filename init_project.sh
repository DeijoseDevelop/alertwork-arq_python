#! /bin/bash

pipenv install -r requirements.txt
pipenv shell
python manage.py makemigrations
python manage.py migrate
docker run -d rabbitmq:3
celery -A alert_work worker -l info & celery -A alert_work beat -l info & python manage.py runserver