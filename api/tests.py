from django.test import TestCase
from .models import User, Task
from .views import ApiUsers
from rest_framework.test import APIRequestFactory
from rest_framework.test import force_authenticate
from consumer.views import DetailUser
from rest_framework.test import APITestCase
from django.urls import reverse
from rest_framework import status

class TestApi(TestCase):
    url_users = '/api/users/'
    url_task = '/api/task/'
    url_task_put = '/api/task/1'
    data_init_api = {
        "id": 1,
        "title": 'hello',
        "description": 'jehdfhhedbuiewbhdiue',
        "init_date": '2022-04-27',
        "end_date": '2022-04-27',
        "user": 1
    }
    data_api_put = {
        "id": 1,
        "title": 'hello',
        "description": 'jehdfhhedbuiewbhdiue',
        "init_date": '2022-04-27',
        "end_date": '2022-04-27',
        "user": 1
    }
    data_api_put_2 = {
        "id": 1,
        "title": 'arquitectura',
        "description": 'jehdfhhedbuiewbhdiue',
        "init_date": '2022-04-27',
        "end_date": '2022-04-27',
        "user": 1
    }

    def test_api_users_get(self):
        response = self.client.get(self.url_users)
        self.assertEqual(response.status_code, 200)

    def test_api_task_post(self):
        response = self.client.get(self.url_task)
        self.assertEqual(response.status_code, 200)

    def validate_with_user_logged(self):
        # user1 test
        user1 = User.objects.create(username='test1')
        user1.set_password('testuser1')
        user1.save()

        # user2 test
        user2 = User.objects.create(username='test2')
        user2.set_password('testuser2')
        user2.save()

        self.client.login(username='test1', password='testuser1')
        response = self.client.post(
            path=self.url_task, data=self.data_init_api)
        self.assertEqual(response.status_code, 200)
        self.client.logout()

        self.client.login(username='test1', password='testuser1')
        response = self.client.put(path=self.url_task + '1', data=self.data_init_api,
                                   content_type='application/json')
        self.assertEqual(response['id'], self.data_api_put['id'])
        self.assertEqual(response.status_code, 200)
        self.client.logout()

        self.client.login(username='test2', password='testuser2')
        response = self.client.put(path=self.url_task + '1', data=self.data_init_api,
                                   content_type='application/json')

        self.assertEqual(response.status_code, 403)
        self.client.logout()


class TestViewApi(TestCase):
    def test_views_api(self):

        # Using the standard RequestFactory API to create a form POST request
        fake_task ={
            "id": 1,
            "title": 'hello',
            "description": 'jehdfhhedbuiewbhdiue',
            "init_date": '2022-04-27',
            "end_date": '2022-04-27',
            "user": 1
        }
        fake_task_put = {
            "id": 1,
            "title": 'vesv',
            "description": 'rvrdvrvr',
            "init_date": '2022-04-27',
            "end_date": '2022-04-27',
            "user": 1
        }
        factory = APIRequestFactory()
        request = factory.post('/api/task/', fake_task)

        response = ApiUsers.as_view()(request)

        factory = APIRequestFactory()
        request = factory.put('/api/task/1', fake_task_put)

        response = ApiUsers.as_view()(request)
        
    def test_authentication(self):
        fake_user = User.objects.create(username='test1')
        fake_user.set_password('password')
        fake_user.save()
        self.client.login(username='test1', password='password')
        factory = APIRequestFactory()
        user = User.objects.get(username='test1')
        view = DetailUser.as_view()

        # Make an authenticated request to the view...
        #request = factory.get('/login/')
        #force_authenticate(request, user=user)
        #response = view(request)

class TestRest(APITestCase):
    def test_rest(self):
        user1 = User.objects.create(username='test1')
        user1.set_password('testuser1')
        user1.save()
        self.client.login(username='test1', password='testuser1')
        url = reverse('api:api-tasks')
        data = {
            "id": 1,
            "title": 'hello',
            "description": 'jehdfhhedbuiewbhdiue',
            "init_date": '2022-04-27',
            "end_date": '2022-04-27',
            "user": user1.id
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['title'], data['title'])
        self.assertEqual(Task.objects.count(), 1)
        self.assertEqual(Task.objects.get().title, data['title'])

