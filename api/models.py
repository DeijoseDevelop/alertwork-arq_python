from django.db import models
from django.contrib.auth.models import AbstractUser

class User(AbstractUser):
    pass


class Task(models.Model):
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name='task_user')
    title = models.CharField(max_length=50)
    description = models.TextField(null=True, blank=True)
    init_date = models.DateField(auto_now=True)
    end_date = models.DateField()

    def __str__(self):
        return self.title
