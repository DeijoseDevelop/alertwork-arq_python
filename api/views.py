from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django.http import Http404
from .models import User, Task
from .serializers import UserSerializer,TaskSerializer
#from .models import Task


class ApiUsers(APIView):
    def get(self, request):
        users = User.objects.all()
        serializer_class = UserSerializer(users, many=True)
        data = {
            'users': serializer_class.data,
        }
        return Response(data)

    def post(self, request):
        serializer_class = UserSerializer(data=request.data)
        if serializer_class.is_valid():
            serializer_class.save()
            return Response(serializer_class.data)
        else:
            return Response(serializer_class.errors)

class ApiUser(APIView):
    def get_object(self, pk):
        try:
            return User.objects.get(pk=pk)
        except User.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        user= self.get_object(pk)
        serializer_class = UserSerializer(user)
        return Response(serializer_class.data)


    def put(self, request, pk):
        user = User.objects.get(pk=pk)
        serializer_class = UserSerializer(user,data=request.data)
        if serializer_class.is_valid():
            serializer_class.save()
            return Response((serializer_class.data))
        else:
            return Response(serializer_class.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        user = User.objects.get(pk=pk)
        user.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

class ApiTasks(APIView):
    def get(self, request):
        tasks = Task.objects.all()
        serializer_class = TaskSerializer(tasks , many=True)
        data = {
            'Tasks': serializer_class.data,
        }
        return Response(data)

    def post(self, request):
        serializer_class = TaskSerializer(data=request.data)
        if serializer_class.is_valid():
            serializer_class.save()
            return Response(serializer_class.data)
        else:
            return Response(serializer_class.errors)

class ApiTask(APIView):
    def get_object(self, pk):
        try:
            return Task.objects.get(pk=pk)
        except Task.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        user= self.get_object(pk)
        serializer_class = TaskSerializer(user)
        return Response(serializer_class.data)


    def put(self, request,pk):
        user = Task.objects.get(pk=pk)
        serializer_class = TaskSerializer(user,data=request.data)
        if serializer_class.is_valid():
            serializer_class.save()
            return Response((serializer_class.data))
        else:
            return Response(serializer_class.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        user = Task.objects.get(pk=pk)
        user.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)