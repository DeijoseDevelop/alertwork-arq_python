from rest_framework.serializers import ModelSerializer
from .models import Task, User


class TaskSerializer(ModelSerializer):
    class Meta:
        model = Task
        fields = ['id', 'title', 'description', 'init_date', 'end_date','user']

class UserSerializer(ModelSerializer):
    task_user = TaskSerializer(many=True, read_only=True)
    class Meta:
        model = User
        fields = ['id', 'username', 'email', 'task_user']