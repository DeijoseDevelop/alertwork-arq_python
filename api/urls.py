from django.urls import path
from .views import ApiUsers, ApiUser,ApiTasks,ApiTask

app_name = 'api'

urlpatterns = [
    path('users/', ApiUsers.as_view(), name='api_users'),
    path('users/<int:pk>', ApiUser.as_view(), name='api_user'),
    path('task/', ApiTasks.as_view(), name='api-tasks'),
    path('task/<int:pk>', ApiTask.as_view(), name='api-task'),
]