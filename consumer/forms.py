
from django import forms
from api.models import User, Task
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm


class LoginForm(AuthenticationForm):
    username = forms.CharField(label='Username',
                               max_length=30,
                               widget=forms.TextInput(
                                   attrs={'class': 'form-control',
                                          'placeholder': 'Enter Your username'}))
    password = forms.CharField(label='Password',
                               max_length=30,
                               widget=forms.PasswordInput(
                                   attrs={'class': 'form-control',
                                            'placeholder': 'Enter Your password'}))

class RegisterForm(UserCreationForm):
    username = forms.CharField(label='Username',
                               max_length=30,
                               widget=forms.TextInput(
                                   attrs={'class': 'form-control',
                                          'placeholder': 'Enter Your username'}))
    email = forms.EmailField(label='Email',
                             widget=forms.EmailInput(
                                 attrs={'class': 'form-control',
                                        'placeholder': 'Enter your email'}))
    first_name = forms.CharField(label='First Name',
                                 max_length=30,
                                 widget=forms.TextInput(
                                        attrs={'class': 'form-control',
                                               'placeholder': 'Enter your first name'}))
    last_name = forms.CharField(label='Last Name',
                                 max_length=30,
                                 widget=forms.TextInput(
                                     attrs={'class': 'form-control',
                                            'placeholder': 'Enter your last name'}))

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email')


class CreateTask(forms.ModelForm):
    title = forms.CharField(label='Title',
                            max_length=30,
                            widget=forms.TextInput(
                                attrs={'class': 'form-control',
                                        'placeholder': 'Enter your title'}))
    description = forms.CharField(label='Description',
                                  max_length=30,
                                  widget=forms.Textarea(
                                        attrs={'class': 'form-control',
                                                  'placeholder': 'Enter your description'}))
    end_date = forms.DateField(label='End Date',
                               widget=forms.DateInput(
                                      attrs={'class': 'form-control input-element',
                                                'placeholder': 'YYYY-MM-DD'}))
    class Meta:
        model = Task
        fields = ('title', 'description', 'end_date')