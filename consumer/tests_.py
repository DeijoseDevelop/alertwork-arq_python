from .tasks import show_email, send_email_tasks
from django.test import TestCase
from unittest import mock

# Create your tests here.

class TestMockValue(TestCase):

    def test_show_email_type(self):
        result = show_email()
        print(result)
        assert type(result) == list, "invalid answer"
        print("correct data type")



    def test_empty_list(self):
        result = show_email()
        assert len(result) == 0, "has no records"
        print("has record")

