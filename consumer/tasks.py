from __future__ import absolute_import, unicode_literals
from datetime import datetime
from api.models import User
from django.core.mail import send_mail
from alert_work import settings
from celery import shared_task




def  show_email():
    list_user = []
    users = User.objects.all()
    for user in users:
        tasks_ = user.task_user.all()
        for _task in tasks_:
            if str(_task.end_date) == datetime.today().strftime("%Y-%m-%d"):
                list_user.append(user)
    return list_user


@shared_task
def send_email_tasks():
    asunto = 'Reminder'
    mensaje = 'the following tasks are due today'
    users = show_email()
    for user in users:
        send_mail(asunto, mensaje, settings.EMAIL_HOST_USER, [user.email], fail_silently=False)
    return users