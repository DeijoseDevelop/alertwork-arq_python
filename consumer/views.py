from django.shortcuts import render, redirect
from django.views.generic import FormView, DetailView
from django.views.generic import RedirectView, CreateView
from .forms import LoginForm, RegisterForm, CreateTask
from django.urls import reverse_lazy
from django.contrib.auth import authenticate, login, logout
#from django.shortcuts import get_object_or_404
from api.models import User, Task
from django.contrib.auth.mixins import LoginRequiredMixin
import requests
from django.contrib.auth.decorators import login_required
from datetime import datetime
from utils.scrapper import get_scrapp

class LoginView(FormView):
    form_class = LoginForm
    template_name = 'consumer/login.html'
    success_url = reverse_lazy('users:detail_user')

    def form_valid(self, form):
        username = form.cleaned_data['username']
        password = form.cleaned_data['password']
        user = authenticate(username=username, password=password)
        if user is not None and user.is_active:
            login(self.request, user)
            return super(LoginView, self).form_valid(form)
        else:
            return self.form_invalid(form)

    def get_success_url(self):
        return reverse_lazy('users:detail_user', kwargs={'pk': self.request.user.pk})


class DetailUser(LoginRequiredMixin, DetailView):
    model = User
    template_name = 'consumer/detail_user.html'

    def get_context_data(self, **kwargs):
        context = super(DetailUser, self).get_context_data(**kwargs)
        user = requests.get(
            f'http://localhost:8000/api/users/{self.request.user.id}')
        obj = user.json()
        tasks = user.json()['task_user']
        context['user'] = obj
        context['tasks'] = tasks
        context['scrapp'] = get_scrapp()
        return context


class LogoutView(LoginRequiredMixin, RedirectView):
    url = reverse_lazy('users:login')

    def get(self, request, *args, **kwargs):
        logout(request)
        return super(LogoutView, self).get(request, *args, **kwargs)


class RegisterView(CreateView):
    form_class = RegisterForm
    model = User
    template_name = 'consumer/register.html'
    success_url = reverse_lazy('users:login')

    def form_valid(self, form):
        form.save()
        return super(RegisterView, self).form_valid(form)


@login_required
def create_task(request):
    tasks = requests.get('http://localhost:8000/api/task')
    if request.method == 'POST':
        form = CreateTask(request.POST)
        title = request.POST['title']
        description = request.POST['description']
        end_date = request.POST['end_date']
        user_id = request.user.id
        id_task = len(tasks.json()['Tasks']) + 1
        data = {
            "id": id_task,
            "title": f"{title}",
            "description": f"{description}",
            "init_date": f"{datetime.today().strftime('%Y-%m-%d')}",
            "end_date": f"{end_date}",
            "user": user_id
        }
        requests.post(
            f'http://localhost:8000/api/task/', data=data)
        return redirect(reverse_lazy('users:detail_user', kwargs={'pk': request.user.pk}))
    else:
        form = CreateTask()

    return render(request, 'consumer/create_task.html', {'form': form})

@login_required
def update_task(request, pk):
    if request.method == 'POST':
        task = requests.get('http://localhost:8000/api/task/' + str(pk))
        form = CreateTask(request.POST)
        title = request.POST['title']
        description = request.POST['description']
        end_date = request.POST['end_date']
        user_id = request.user.id
        task_id = task.json()['id']
        data = {
            "id": task_id,
            "title": f"{title}",
            "description": f"{description}",
            "init_date": f"{datetime.today().strftime('%Y-%m-%d')}",
            "end_date": f"{end_date}",
            "user": user_id
        }
        requests.put(
            f'http://localhost:8000/api/task/{task_id}', data=data)
        return redirect(reverse_lazy('users:detail_user', kwargs={'pk': request.user.pk}))
    else:
        form = CreateTask(instance=task)
    return render(request, 'consumer/update_task.html', {'form': form})

@login_required
def delete_task(request, pk):
    task = requests.get('http://localhost:8000/api/task/' + str(pk))
    task_id = task.json()['id']
    requests.delete(f'http://localhost:8000/api/task/{task_id}')
    return redirect(reverse_lazy('users:detail_user', kwargs={'pk': request.user.pk}))


@login_required
def detail_task(request, pk):
    task = requests.get(
        f'http://localhost:8000/api/task/{pk}')
    return render(request, 'consumer/detail_task.html', {'task': task.json(), 'user': request.user})